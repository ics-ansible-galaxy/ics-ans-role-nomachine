import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_nomachine_installed(host):
    assert host.package("nomachine").is_installed


def test_nxserver_running(host):
    assert host.service("nxserver").is_running
